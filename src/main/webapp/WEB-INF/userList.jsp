
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
</head>
<body>
<h2>Welcome, ${username}</h2>
<!--provide an html table start tag -->
<div align="center">
    list of all users
<table style="border: 1px solid;">
    <!-- iterate over the collection using forEach loop -->
    <%--@elvariable id="users" type="java.util.List"--%>

    <tr>
        <th>id</th>
        <th>username</th>
        <th>nickname</th>
    </tr>

    <c:forEach items="${users}" var="user">

        <!-- create an html table row -->
        <tr>
            <!-- create cells of row -->
            <td>${user.iduser}</td>
            <td>${user.username}</td>
            <td>${user.name}</td>
            <td>
                <form onsubmit="return confirmDelete()" action="/remove" method="post">
                    <input type="hidden" name="selectedusername" value="${user.username}">
                    <input type="submit" value="delete user">
                    <script type='text/javascript'>
                        function confirmDelete() {
                            return confirm("Are you sure?");
                        }
                    </script>
                </form>
            </td>
            <td>
                <form onsubmit="return confirmEdit()" action="/edit" method="get">
                    <input type="hidden" name="selectedusername" value="${user.username}">
                    <input type="submit" value="edit">
                    <script type='text/javascript'>
                        function confirmEdit() {
                            return confirm("You're about to edit a user, are you sure?");
                        }
                    </script>
                </form>
            </td>
            <!-- close row -->
        </tr>
        <!-- close the loop -->
    </c:forEach>
    <!-- close table -->
</table>
    <form action="/logout" method="post">
        <input type="submit" value="LOGOUT">
    </form>
    <form action="/registration" method="get">
        <input type="submit" value="add User">
    </form>
    </div>
</body>
</html>
