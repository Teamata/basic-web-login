/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.muic.ooc.webapp.service;

import java.io.IOException;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.muic.ooc.webapp.database.UserDatabase;

/**
 *
 * @author gigadot
 */
public class SecurityService {

//    private Map<String, String> userCredentials = new HashMap<String, String>() {{
//        put("admin", "123456");
//        put("muic", "1111");
//    }};
    
    public boolean isAuthorized(HttpServletRequest request) {
        String username = (String) request.getSession()
                .getAttribute("username");
       return checkUserExist(username);
    }

    public boolean checkUserExist(String username){
        UserDatabase database = new UserDatabase();
        return database.findUser(username);
    }
    
    public boolean authenticate(String username, String password, HttpServletRequest request) {
        UserDatabase database = new UserDatabase();
        boolean found = database.validUser(username,password);
        if(found){
            request.getSession().setAttribute("username", username);
            return true;
        }
        else{
            return false;
        }
    }
    
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("log out!");
        request.getSession().invalidate();
        request.removeAttribute("user");
        try {
            response.sendRedirect("/index.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
