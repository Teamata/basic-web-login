package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.database.User;
import io.muic.ooc.webapp.database.UserDatabase;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teama on 3/6/2018.
 */
public class UserListServlet extends HttpServlet implements Routable{

    private SecurityService securityService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            System.out.println("authorized from userList");
            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);
            displayUser(request,response);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/userList.jsp");
            rd.include(request, response);
            System.out.println("in user list page");
        }
        else{
            response.sendRedirect("/login");
        }
    }

    private void displayUser(HttpServletRequest request, HttpServletResponse response) throws IOException{
        List<User> users = new ArrayList<>();
        UserDatabase user_db = new UserDatabase();
        Connection con = null;
        try {
            con = user_db.connect();
            String sql = "SELECT * FROM user_table";
            PreparedStatement st = con.prepareStatement(sql);
            ResultSet results = st.executeQuery();
//            System.out.println("retrieving data from database..");
            while(results.next()){
                User us = new User();
                us.setIdUser(results.getInt("iduser"));
                us.setUsername(results.getString("user"));
                us.setPassword(results.getString("password"));
                us.setName(results.getString("name"));
//                System.out.println(us.getUser_id() + " " + us.getUsername() + " " + us.getPassword());
                users.add(us);
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        request.setAttribute("users",users);
    }

    //Edit, delete user.
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
//        System.out.println("test");
//        List<User> users = new ArrayList<>();
//        UserDatabase user_db = new UserDatabase();
//        Connection con;
//        try {
//            con = user_db.connect();
//            String sql = "SELECT * FROM user";
//            PreparedStatement st = con.prepareStatement(sql);
//            ResultSet results = st.executeQuery();
//            System.out.println("retrieving data from database..");
//            System.out.println(results.next());
//            while(results.next()){
//                System.out.println("test");
//                User us = new User();
//                us.setUser_id(results.getInt("iduser"));
//                us.setUsername(results.getString("user"));
//                us.setPassword(results.getString("password"));
//                System.out.println(us.getUser_id() + " " + us.getUsername() + " " + us.getPassword());
//                users.add(us);
//            }
//        } catch (ClassNotFoundException | SQLException e) {
//            e.printStackTrace();
//        }
//        request.setAttribute("users",users);
//    }


    @Override
    public String getMapping() {
        return "/userList";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
