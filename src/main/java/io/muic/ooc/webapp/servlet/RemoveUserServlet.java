package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.database.User;
import io.muic.ooc.webapp.database.UserDatabase;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Teama on 3/7/2018.
 */
public class RemoveUserServlet extends HttpServlet implements Routable{

    SecurityService securityService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            System.out.println("authorized from remove");
            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);
            System.out.println("in remove page");
        }
        else{
            response.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        System.out.println("In delete user!");
        String username = (String) request.getSession().getAttribute("username");
        request.setAttribute("username", username);
        String selectedName = request.getParameter("selectedusername");
        if(securityService.isAuthorized(request) && !username.equals(selectedName)) {
            UserDatabase db = new UserDatabase();
            db.removeUser(selectedName);
        }
        response.sendRedirect("/userList");
    }

    @Override
    public String getMapping() {
        return "/remove";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
