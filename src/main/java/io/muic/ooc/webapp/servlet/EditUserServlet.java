package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.database.User;
import io.muic.ooc.webapp.database.UserDatabase;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Teama on 3/7/2018.
 */

public class EditUserServlet extends HttpServlet implements Routable {

    private Integer edit_uid;
    private Integer uid;
    private String e_uname;

    private SecurityService securityService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            System.out.println("authorized from edit");
            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);
            e_uname = request.getParameter("selectedusername");
            UserDatabase user_db = new UserDatabase();
            User user = user_db.getUserbyName(e_uname);
            edit_uid = user_db.findIDUser(e_uname);
            uid = user_db.findIDUser(username);
            request.setAttribute("user",user);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/edit.jsp");
            rd.include(request, response);
            System.out.println("in edit page");
        }
        else{
            response.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        System.out.println("in edit section!");
        UserDatabase user_db = new UserDatabase();
        String name = request.getParameter("user");
        String password = request.getParameter("password");
        String nickname = request.getParameter("name");
        System.out.println(name);
        if(securityService.isAuthorized(request)){
            boolean success = user_db.editUser(name,password, nickname,edit_uid);
            String username = (String) request.getSession().getAttribute("username");
            System.out.println("username " + username);
            System.out.println("edit success!");
            if(success) {
                request.getSession().setAttribute("username", username);
            if(uid.equals(edit_uid)) {
                request.getSession().setAttribute("username", name);
            }
            response.sendRedirect("/userList");
        }
        else{
            String error = "Username or password is missing or that username is already taken";
            request.setAttribute("selectedusername", e_uname);
            request.setAttribute("error", error);
            user_db = new UserDatabase();
            System.out.println(e_uname);
            User user = user_db.getUserbyName(e_uname);
            edit_uid = user_db.findIDUser(e_uname);
            request.setAttribute("user",user);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/edit.jsp");
            try {
                rd.include(request, response);
            } catch (ServletException e) {
                e.printStackTrace();
                }
            }
        }
        else{
            System.out.println("not authenticate");
            response.sendRedirect("/login");
        }
    }

    @Override
    public String getMapping() {
        return "/edit";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
