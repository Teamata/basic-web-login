package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.database.UserDatabase;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RegistrationServlet extends HttpServlet implements Routable{

    private SecurityService securityService;

    private static final long serialVersiononUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        boolean authorized = securityService.isAuthorized(request);
        if (authorized) {
            System.out.println("authorized from registration");
            String username = (String) request.getSession().getAttribute("username");
            request.setAttribute("username", username);
//            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
//            rd.include(request, response);
//            System.out.println("in register page");
        }
//        else{
//            response.sendRedirect("/login");
//        }
        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
        rd.include(request, response);
        System.out.println("in register page");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try {
            String name = request.getParameter("user");
            String password = request.getParameter("password");
            String nickname = request.getParameter("name");
            UserDatabase user_db = new UserDatabase();
            if(!securityService.checkUserExist(name)) {
                boolean success = user_db.addUser(name,password,nickname);
                if(success){
                    System.out.println("success!");
                    if(securityService.isAuthorized(request)){
                        System.out.println("same user");
                        response.sendRedirect("/userList");
                    }
                    else {
                        response.sendRedirect("/login");
                    }
                }
            }
            else {
                System.out.println("invalid user");
                String error = "Username or password is missing. or this user is already taken";
                request.setAttribute("error", error);
                RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
                rd.include(request, response);
            }
            //invalid user already exist!
            } catch(ClassNotFoundException | ServletException e){
                e.printStackTrace();
        }
    }

    @Override
    public String getMapping() {
        return "/registration";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
