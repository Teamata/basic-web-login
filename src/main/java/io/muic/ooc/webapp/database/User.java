package io.muic.ooc.webapp.database;

/**
 * Created by Teama on 3/6/2018.
 */

public class User {

    private Integer idUser;
    private String username;
    private String password;
    private String name;

    public User(){
        idUser = null;
        username = null;
        password = null;
        name = null;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setIdUser(Integer user_id) {
        this.idUser = user_id;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public Integer getIduser() {
        return idUser;
    }
}
