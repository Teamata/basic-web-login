package io.muic.ooc.webapp.database;

import io.muic.ooc.webapp.service.HashPassword;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.List;


/**
 * Created by Teama on 3/6/2018.
 */
public class UserDatabase {

    private final String url = "jdbc:mysql://database:3306/user_table?useSSL=false";
    private Connection connection;
    private String user;
    private String password;
    private String table = "user_table";

    public UserDatabase(){
        user = "ooc";
        password = "123456789";

    }

    public Connection connect() throws ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void closeConnection() throws SQLException {
        connection.close();
    }

    public boolean editUser(String newUsername, String newPassword, String nickname,Integer uid){

        PreparedStatement pt = null;
        Connection con = null;
        if(!findUser(newUsername) || getUserbyName(newUsername).getIduser().equals(uid)) {
            try {
                System.out.println(newUsername);
                System.out.println(newPassword);
                System.out.println(uid);
                String sql = "UPDATE "+table+" SET user = ?"
                        + ", password = ? "
                        + ", name = ? "
                        + "WHERE iduser=" + uid;
                con = this.connect();
                pt = con.prepareStatement(sql);
                String hashedPassword = HashPassword.hashPassword(newPassword);
                pt.setString(1, newUsername);
                pt.setString(2, hashedPassword);
                pt.setString(3, nickname);
                pt.executeUpdate();
                con.close();
                return true;
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            return false;
        }
        else{
            return false;
        }
    }

    public boolean addUser(String username, String password, String nickname) throws ClassNotFoundException {
        String sql = "INSERT INTO "+table+"(user,password,name) values(?,?,?)";
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection = null;
        try{
            connection = this.connect();
            String hashedPassword = HashPassword.hashPassword(password);
            System.out.println("Connection created successfully!");
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, hashedPassword);
            statement.setString(3, nickname);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(connection!=null){
                try {
                    this.closeConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public boolean validUser(String username, String password){
        PreparedStatement pt = null;
        ResultSet rs = null;
        Connection con = null;
        try{
            String sql = "SELECT * FROM "+table+" WHERE user=?";
            con = this.connect();
            pt = con.prepareStatement(sql);
            pt.setString(1, username);
            rs = pt.executeQuery();
            while(rs.next()){
                String user_table = rs.getString("user");
                String password_table = rs.getString("password");
                if(user_table.equals(username) && HashPassword.verifyPassword(password_table,password)){
                    System.out.println("found valid user!");
                    this.closeConnection();
                    return true;
                }
            }
            this.closeConnection();
        }
        catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        } finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("User not found, or wrong password");
        return false;
    }

    public boolean findUser(String username){
        PreparedStatement pt = null;
        ResultSet rs = null;
        Connection con = null;
        try{
            String sql = "SELECT * FROM "+table+" WHERE user=?";
            con = this.connect();
            pt = con.prepareStatement(sql);
            pt.setString(1, username);
            rs = pt.executeQuery();
            System.out.println("try to find : " + username);
            while(rs.next()){
                System.out.println("HELLO");
                String user_table = rs.getString("user");
                if(user_table.equals(username)){
                    System.out.println("found valid user!");
                    this.closeConnection();
                    return true;
                }
            }
            con.close();
        }
        catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        } finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Couldn't find user");
        return false;
    }

    public Integer findIDUser(String username){
        PreparedStatement pt = null;
        ResultSet rs = null;
        Connection con = null;
        try{
            String sql = "SELECT * FROM "+table+" WHERE user=?";
            con = this.connect();
            pt = con.prepareStatement(sql);
            pt.setString(1, username);
            rs = pt.executeQuery();
            while(rs.next()){
                String user_table = rs.getString("user");
                if(user_table.equals(username)){
                    Integer i = rs.getInt("iduser");
                    this.closeConnection();
                    return i;
                }
            }
            con.close();
        }
        catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        } finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("User doesn't exist");
        return null;
    }

    public User getUserbyName(String username){
        PreparedStatement pt = null;
        ResultSet rs = null;
        Connection con = null;
        User user = null;
        try{
            String sql = "SELECT * FROM "+table+" WHERE user=?";
            con = this.connect();
            pt = con.prepareStatement(sql);
            pt.setString(1, username);
            rs = pt.executeQuery();
            while(rs.next()){
                String user_table = rs.getString("user");
                if(user_table.equals(username)){
                    user = new User();
                    user.setIdUser(rs.getInt("iduser"));
                    user.setUsername(rs.getString("user"));
                    user.setName(rs.getString("name"));
                    System.out.println("found valid user!");
                    this.closeConnection();
                    return user;
                }
            }
            con.close();
        }
        catch(SQLException | ClassNotFoundException e){
            e.printStackTrace();
        } finally {
            if(con!=null){
                try {
                    con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("User doesn't exist");
        return null;
    }

    public boolean removeUser(String username){
        PreparedStatement pt = null;
        if(findUser(username)){
            try {
                String sql = "DELETE FROM "+table+" WHERE user= ?;";
                pt = this.connect().prepareStatement(sql);
                pt.setString(1, username);
                pt.executeUpdate();
                return true;
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public Connection getConnection() {
        return connection;
    }
}
