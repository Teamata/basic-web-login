FROM openjdk:8u151-jdk-slim
WORKDIR /app
ADD . .
EXPOSE 8082
CMD ["java","-jar","target/login-webapp-1.0-SNAPSHOT.jar"]
